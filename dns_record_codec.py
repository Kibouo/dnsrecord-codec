import struct
import socket
from typing import Union

# dnsRecord util functions
# format layout: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dnsp/6912b338-5472-4f59-b912-0edb536b6ed8

# https://en.wikipedia.org/wiki/List_of_DNS_record_types
DNS_TYPES_BY_NR = { 0: 'NULL', 1: 'A', 2: 'NS', 5: 'CNAME', 6: 'SOA', 12: 'PTR', 13: 'HINFO', 15: 'MX', 16: 'TXT', 17: 'RP', 18: 'AFSDB', 24: 'SIG', 25: 'KEY', 28: 'AAAA', 29: 'LOC', 33: 'SRV', 35: 'NAPTR', 36: 'KX', 37: 'CERT', 39: 'DNAME', 42: 'APL', 43: 'DS', 44: 'SSHFP', 45: 'IPSECKEY', 46: 'RRSIG', 47: 'NSEC', 48: 'DNSKEY', 49: 'DHCID', 50: 'NSEC3', 51: 'NSEC3PARAM', 52: 'TLSA', 53: 'SMIMEA', 55: 'HIP', 59: 'CDS', 60: 'CDNSKEY', 61: 'OPENPGPKEY', 62: 'CSYNC', 63: 'ZONEMD', 64: 'SVCB', 65: 'HTTPS', 108: 'EUI48', 109: 'EUI64', 249: 'TKEY', 250: 'TSIG', 256: 'URI', 257: 'CAA', 32768: 'TA', 32769: 'DLV' }
DNS_TYPE_NR_BY_TYPE = {v: k for k, v in DNS_TYPES_BY_NR.items()}

def decode(record: bytearray):
    '''
    Unpack the values from a dnsRecord stored as binary data.
    '''
    def decode_domain_string(data: bytearray) -> str:
        result = ''
        # amount_encoded_bytes = data[:1]
        amount_domain_parts = struct.unpack('B', data[1:2])[0]
        data = data[2:]

        for _ in range(amount_domain_parts):
            part_len = struct.unpack('B', data[:1])[0]
            result += data[1:1+part_len].decode('utf-8')
            result += '.' # don't forget to join the domain parts
            data = data[1+part_len:]

        if data != b'\0':
            raise ValueError("ERROR: Encoded data did not end with null-byte.")
        return result

    data_len, dns_type, version, rank, flags, serial = struct.unpack('H H B B H I', record[:12])
    ttl = struct.unpack('!I', record[12:16])[0]
    reserved, timestamp = struct.unpack('I I', record[16:24])
    data = record[24:]

    print(f"Data length: {data_len}")
    print(f"DNS type: {hex(dns_type)} ({DNS_TYPES_BY_NR.get(dns_type)})")
    if version != 0x05:
        raise ValueError("ERROR: Version should be '5', got '{}'".format(version))
    print(f"Rank: {rank}")
    print(f"Flags: {flags}")
    if flags != 0x0:
        raise ValueError("ERROR: Flags should be '0', got '{}'".format(flags))
    print(f"Serial: {serial}")
    print(f"TTL: {ttl}")
    if reserved != 0x0:
        print("ERROR: Reserved should be '0', got '{}'".format(reserved))
    print(f"Timestamp: {timestamp}")
    if len(data) != data_len:
        raise ValueError("ERROR: Length of data should be '{}', got '{}'".format(data_len, len(data)))

    # Only most prominent types handled
    if dns_type == 0x0:
        print("Data: None")
    elif dns_type == 0x1:
        print(f"Data: {socket.inet_ntop(socket.AF_INET, data)}")
    elif dns_type == 0x2 \
            or dns_type == 0x5 \
            or dns_type == 0x6 \
            or dns_type == 0xc \
            or dns_type == 0xf \
            or dns_type == 0x10:
        print(f"Data: {decode_domain_string(data)}")
    elif dns_type == 0x1c:
        print(f"Data: {socket.inet_ntop(socket.AF_INET6, data)}")
    else:
        print("WARNING: DNS type not handled.")
        print(f"Data: {data}")


def encode(dns_type: Union[int, str], serial_nr, data: str, ttl = 9999) -> bytearray:
    '''
    Create/spoof a simple dnsRecord.
    '''
    def encode_domain_string(data: str) -> bytearray:
        result = b''
        parts = data.split('.') # remove `.` between parts of the domain

        for part in parts:
            # each part of the domain if prepended by its length
            result += struct.pack('B', len(part))
            result += part.encode('utf-8')
        result + b'\0' # indicate end of str

        # prepend more meta data: amount of encoded bytes & amount of domain parts
        return struct.pack('B', len(result)) + struct.pack('B', len(parts)) + result

    if type(dns_type) == str:
        try:
            dns_type = DNS_TYPE_NR_BY_TYPE[dns_type]
        except:
            raise ValueError("ERROR: Unknown DNS type {}".format(dns_type))

    # Only most prominent types handled
    if dns_type == 0x0:
        data = data.encode('utf-8')
    elif dns_type == 0x1:
        data = socket.inet_pton(socket.AF_INET, data)
    elif dns_type == 0x2 \
            or dns_type == 0x5 \
            or dns_type == 0x6 \
            or dns_type == 0xc \
            or dns_type == 0xf \
            or dns_type == 0x10:
        data = encode_domain_string(data)
    elif dns_type == 0x1c:
        data = socket.inet_pton(socket.AF_INET6, data)
    else:
        print("WARNING: DNS type not handled.")
        data = data.encode('utf-8')

    data_len = len(data)
    version = 0x05 # fixed value
    rank = 0x0 # TODO: does value matter?
    flags = 0x0000 # fixed value
    reserved = 0x00000000 # fixed value
    timestamp = 0x0 # last update in hours

    record = struct.pack('H H B B H I', data_len, dns_type, version, rank, flags, serial_nr)
    record += struct.pack('!I', ttl)
    record += struct.pack('I I', reserved, timestamp)
    record += data

    return record
